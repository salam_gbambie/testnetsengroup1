﻿using System;
using System.ComponentModel;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Windows.Input;
using SQLite;
using Xamarin.Forms;
using System.Diagnostics;

namespace Notes.Models
{
    public class Note
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }


        public ICommand SaveCommand { get; private set; }
        public ICommand DeleteCommand { get; private set; }

        public Note()
        {

            SaveCommand = new Command<Note>(async (o) => await SaveMethod(this));

            async Task SaveMethod(Note o)
            {
                this.Date = DateTime.UtcNow;
                await App.Database.SaveNoteAsync(this);
                await App.Current.MainPage.Navigation.PopAsync();

            }

            DeleteCommand = new Command<Note>(async (o) => await DeleteMethod(this));

            async Task DeleteMethod(Note o)
            {
                var answer = await Application.Current.MainPage.DisplayAlert("Question?", "Voulez vous supprimer la note?", "Oui", "Non");
                if (answer)
                {
                    await App.Database.DeleteNoteAsync(o);
                    await App.Current.MainPage.Navigation.PopAsync();
                }


            }

        }
    }

   


}