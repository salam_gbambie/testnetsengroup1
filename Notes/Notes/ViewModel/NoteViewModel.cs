﻿using Notes.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Notes.ViewModel
{
    public class NoteViewModel : INotifyPropertyChanged
    {
        public NoteViewModel()
        {
             NoteObject = new Note();
        }

        Command NoteAddedCommandCommand;
        public Command NoteAddedCommand
        {
            get
            {
                return NoteAddedCommandCommand ?? (NoteAddedCommandCommand = new Command(async () => await ExecuteOnEmployeeSave()));
            }
        }

        
        private Note noteObject;
        public Note NoteObject
        {
            get
            {
                return noteObject;
            }
            set
            {
                noteObject = value; OnPropertyChanged();
            }
        }

        public ICommand DeleteCommand { get; private set; }

        #pragma warning disable CS1998 // Cette méthode async n'a pas d'opérateur 'await' et elle s'exécutera de façon synchrone
        private async Task ExecuteOnEmployeeSave()
        #pragma warning restore CS1998 // Cette méthode async n'a pas d'opérateur 'await' et elle s'exécutera de façon synchrone
        {
             await App.Current.MainPage.Navigation.PushAsync(new NoteEntryPage()
             {
                BindingContext = new Note()
             });
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            handler?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public INavigation Navigation { get; internal set; }


        private async Task ExecuteOnNoteAdded()
        {
                await Navigation.PushAsync(new NoteEntryPage()
                {
                    BindingContext = new Note()
                });
        }

    }
}
