﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xamarin.Forms;
using Notes.Models;
using Notes.ViewModel;

namespace Notes
{
    public partial class NotesPage : ContentPage
    {
        //NotesViewModel vm=new NotesViewModel();
        public NotesPage()
        {
            InitializeComponent();
            this.BindingContext = new NoteViewModel();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            listView.ItemsSource = await App.Database.GetNotesAsync();
        }

        //async void OnNoteAddedClicked(object sender, EventArgs e)
        //{
        //    await Navigation.PushAsync(new NoteEntryPage()
        //    {
        //        BindingContext = new Note()
        //    });
        //}

        async void OnListViewItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                await Navigation.PushAsync(new NoteEntryPage()
                {
                    BindingContext = e.SelectedItem as Note
                });
            }
        }
    }
}