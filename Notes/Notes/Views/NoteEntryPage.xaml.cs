﻿using System;
using System.IO;
using Xamarin.Forms;
using Notes.Models;
using System.Threading.Tasks;
using Notes.ViewModel;

namespace Notes
{
    public partial class NoteEntryPage : ContentPage
    {
        public NoteEntryPage()
        {
            InitializeComponent();
           this.BindingContext = new NoteViewModel();
           // BindingContext.Navigation = Navigation;
            //BindingContext = vm;
            //vm.Navigation = Navigation; 

        }

        //async void OnSaveButtonClicked(object sender, EventArgs e)
        //{
        //    var note = (Note)BindingContext;
        //    note.Date = DateTime.UtcNow;
        //    await App.Database.SaveNoteAsync(note);
        //    await Navigation.PopAsync();
        //}

        //async void OnDeleteButtonClicked(object sender, EventArgs e)
        //{
        //    var note = (Note)BindingContext;
        //    await App.Database.DeleteNoteAsync(note);
        //    await Navigation.PopAsync();
        //}
    }
}